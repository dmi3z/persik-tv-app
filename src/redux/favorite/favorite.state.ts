import { FavoritesData } from '@models/core';

export interface FavoritesState {
    favoritesData: FavoritesData;
}
