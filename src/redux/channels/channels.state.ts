import { Tvshow } from './../../app/models/tvshow';
import { Channel, Genre } from '@models/core';

export interface ChannelsState {
  channelsData: {
    channels: Channel[],
    categories: Genre[],
    activeChannel: Channel,
    currentTvshows: Tvshow[]
  };
}
