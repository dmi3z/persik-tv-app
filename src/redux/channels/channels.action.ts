import { Tvshow } from './../../app/models/tvshow';
import { Action } from '@ngrx/store';
import { Channel, Genre } from '@models/core';

// tslint:disable-next-line: no-namespace
export namespace CHANNELS_ACTION {
  export const LOAD_CHANNELS = 'LOAD_CHANNELS';
  export const LOAD_CHANNEL_CATEGORIES = 'LOAD_CHANNEL_CATEGORIES';
  export const SET_ACTIVE_CHANNEL = 'SET_ACTIVE_CHANNEL';
  export const LOAD_CURRENT_TVSHOWS = 'LOAD_CURRENT_TVSHOWS';
}

export class LoadChannels implements Action {
  readonly type = CHANNELS_ACTION.LOAD_CHANNELS;
  constructor(public channels: Channel[]) { }
}

export class LoadCurrentTvshows implements Action {
  readonly type = CHANNELS_ACTION.LOAD_CURRENT_TVSHOWS;

  constructor(public tvshows: Tvshow[]) { }
}

export class LoadChannelCategories implements Action {
  readonly type = CHANNELS_ACTION.LOAD_CHANNEL_CATEGORIES;

  constructor(public categories: Genre[]) { }
}

export class SetActiveChannel implements Action {
  readonly type = CHANNELS_ACTION.SET_ACTIVE_CHANNEL;

  constructor(public activeChannel: Channel) { }
}

export type ChannelsAction = LoadChannels | LoadCurrentTvshows | LoadChannelCategories | SetActiveChannel;
