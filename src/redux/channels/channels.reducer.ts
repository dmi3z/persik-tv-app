import { Tvshow } from './../../app/models/tvshow';
import { Channel, Genre } from '@models/core';
import { CHANNELS_ACTION, ChannelsAction } from './channels.action';

export interface State {
  channels: Channel[];
  categories: Genre[];
  activeChannel: Channel;
  currentTvshows: Tvshow[];
}

const initialState: State = {
  channels: [],
  categories: [],
  activeChannel: null,
  currentTvshows: []
};

export function ChannelsReducer(state = initialState, action: ChannelsAction) {
  switch (action.type) {
    case CHANNELS_ACTION.LOAD_CHANNELS:
      return {
        ...state,
        channels: action.channels
      };
    case CHANNELS_ACTION.LOAD_CURRENT_TVSHOWS:
      return {
        ...state,
        currentTvshows: action.tvshows
      };
    case CHANNELS_ACTION.LOAD_CHANNEL_CATEGORIES:
      return {
        ...state,
        categories: action.categories
      };
    case CHANNELS_ACTION.SET_ACTIVE_CHANNEL:
      return {
        ...state,
        activeChannel: action.activeChannel
      };
    default:
      return state;
  }
}
