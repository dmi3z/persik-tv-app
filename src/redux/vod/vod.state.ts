import { VodCategories } from '@models/core';

export interface VodState {
    vodCategories: VodCategories;
}
