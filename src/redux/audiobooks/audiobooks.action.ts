import { Book } from './../../app/pages/audiobooks/litres.model';
import { Action } from '@ngrx/store';

// tslint:disable-next-line: no-namespace
export namespace AUDIOBOOKS_ACTION {
  export const LOAD_BOOKS = 'LOAD_BOOKS';
  export const PRELOAD_BOOKS = 'PRELOAD_BOOKS';
}

export class LoadAudiobooks implements Action {
  readonly type = AUDIOBOOKS_ACTION.LOAD_BOOKS;

  constructor(public books: Book[]) { }
}

export class PreloadAudiobooks implements Action {
  readonly type = AUDIOBOOKS_ACTION.PRELOAD_BOOKS;

  constructor(public books: Book[]) { }
}

export type AudiobooksAction = LoadAudiobooks | PreloadAudiobooks;
