import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TvGuideComponent } from './tv-guide.component';
import { RouterModule } from '@angular/router';
import { TvGuideChannelsComponent } from './tv-guide-channels/tv-guide-channels.component';
import { NavModule } from 'src/app/directives/nav.module';
import { TvGuideDatesComponent } from './tv-guide-dates/tv-guide-dates.component';
import { DatePipe } from './tv-guide-dates/date.pipe';
import { TvGuideShowsComponent } from './tv-guide-shows/tv-guide-shows.component';
import { TimePipe } from './tv-guide-shows/time.pipe';
import { ShowItemComponent } from './tv-guide-shows/show-item/show-item.component';
import { PreviewComponent } from './preview/preview.component';
import { BoldPipe } from './tv-guide-dates/bold.pipe';

@NgModule({
  declarations: [
    TvGuideComponent,
    TvGuideChannelsComponent,
    TvGuideDatesComponent,
    TvGuideShowsComponent,
    ShowItemComponent,
    PreviewComponent,
    DatePipe,
    TimePipe,
    BoldPipe
  ],
  imports: [
    CommonModule,
    NavModule,
    RouterModule.forChild([
      {
        path: '',
        component: TvGuideComponent
      }
    ])
  ]
})

export class TvGuideModule {}
