import { LoaderService, BackService, PlayerControllerService, PositionMemoryService } from '@services/core';
import { DataService } from '@services/core';
import { Genre, Channel, Tvshow } from '@models/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { ChannelsState } from '../../../redux/channels/channels.state';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tv-guide',
  templateUrl: './tv-guide.component.html',
  styleUrls: ['./tv-guide.component.scss']
})

export class TvGuideComponent implements OnInit, OnDestroy {

  public genres: Genre[] = [];
  public filteredChannels: Channel[] = [];
  public uniqueDates: string[] = [];
  private allTvshows: Tvshow[] = [];
  public tvshows: Tvshow[] = [];
  public currentDate: string;
  public activeChannel: Channel;
  public selectedTvshow: Tvshow;
  public dvr = 0;
  public activeGenre: Genre;

  public switchChannelDelay: any;
  private backServiceSubscriber: Subscription;
  private activatedRouteSubscriber: Subscription;

  constructor(
    private channelsStore: Store<ChannelsState>,
    private dataService: DataService,
    private loaderService: LoaderService,
    private backService: BackService,
    private playerController: PlayerControllerService,
    private memoryService: PositionMemoryService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.backServiceSubscriber = this.backService.backEvent.subscribe(_ => {
      this.playerController.stop();
      this.backService.goToMain();
    });
    this.currentDate = moment().format('YYYY-MM-DD');

    this.activatedRouteSubscriber = this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.genre) {
        this.activeGenre = JSON.parse(params.genre);
      } else {
        this.activeGenre = {
          id: 0,
          is_main: true,
          name: 'Все жанры',
          name_en: ''
        };
      }
      this.onGenreChange();
    });
    /* this.channelSubscriber = this.channelsStore.select('channelsData').subscribe(data => {
        if (this.activeGenre) {
          this.onGenreChange();
        } else {
          this.activeGenre = this.genres[0];
          this.activeChannel = this.memoryService.currentChannel ? this.memoryService.currentChannel : data.channels[0];
          this.filteredChannels = this.channels;
          this.onChannelChange(this.activeChannel);
          this.focusOnFirst();
        }
    }); */
  }

  private get channels(): Channel[] {
    let state: Channel[];
    this.channelsStore.select('channelsData').pipe(take(1)).subscribe(s => state = s.channels);
    return state;
  }

  public onGenreChange(): void {
    if (this.activeGenre.id !== 0) {
      this.filteredChannels = this.channels.filter(ch => ch.genres.includes(this.activeGenre.id));
    } else {
      this.filteredChannels = this.channels;
    }
    this.activeChannel = this.filteredChannels[0];
    if (this.filteredChannels.length > 0) {
      this.onChannelChange(this.filteredChannels[0]);
      this.focusOnFirst();
    }
  }

  public redirectFocus(): void {
    this.focusOnFirst();
  }

  public onChannelChange(channel: Channel): void {
    this.dvr = channel.dvr_sec;
    this.memoryService.currentChannel = channel;
    clearTimeout(this.switchChannelDelay);
    this.switchChannelDelay = setTimeout(() => {
      this.loaderService.startLoading(2);
      this.activeChannel = channel;
      this.dataService.getTvshows(channel.channel_id).then(response => {
        this.allTvshows = response;
        this.uniqueDates = this.getUniqueDates(response.map(item => item.date));
        this.onDateChange(moment().format('YYYY-MM-DD'));
        this.checkFocus();
      }).finally(() => this.loaderService.loadFinished());
      this.dataService.getCurrentTvShows([channel.channel_id]).then(res => {
        this.selectedTvshow = res[0];
      }).finally(() => this.loaderService.loadFinished());
    }, 1000);
  }

  public onDateChange(date: string): void {
    this.currentDate = date;
    this.tvshows = this.allTvshows.filter(item => item.date === this.currentDate);
  }

  public onTvshowFocus(tvshow: Tvshow): void {
    this.selectedTvshow = tvshow;
  }

  private getUniqueDates(dates: string[]): string[] {
    return dates.filter((item, index, self) => self.indexOf(item) === index);
  }

  private checkFocus(): void {
    setTimeout(() => {
      const body = document.body;
      const focusedItem = document.activeElement;
      if (focusedItem === body) {
        const currentTvshow = document.querySelector('.active') as HTMLElement;
        if (currentTvshow) {
          currentTvshow.focus();
        }
      }
    }, 200);
  }

  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 500);
  }

  ngOnDestroy() {
    if (this.backServiceSubscriber) {
      this.backServiceSubscriber.unsubscribe();
    }
    if (this.activatedRouteSubscriber) {
      this.activatedRouteSubscriber.unsubscribe();
    }
  }

}
