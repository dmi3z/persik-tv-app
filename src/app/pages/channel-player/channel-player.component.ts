import { map, take } from 'rxjs/operators';
// import { KeyMap } from './../../../keymaps/keymap';
import { MenuControlService, BackService, TimeService, DataService, LoaderService } from '@services/core';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChannelTapeItemComponent } from './channel-tape/channel-tape-item/channel-tape-item.component';
import { PlayerEvents, ContentName, Tvshow, Channel } from '@models/core';
import { Location } from '@angular/common';
import { Subscription, Observable } from 'rxjs';
import { digitsMap } from 'src/keymaps/keymap';
import { Store } from '@ngrx/store';
import { ChannelsState } from 'src/redux/channels/channels.state';
import { PlayerControllerService } from 'src/app/services/player-controller.service';


@Component({
  selector: 'app-channel-player',
  templateUrl: './channel-player.component.html',
  styleUrls: ['./channel-player.component.scss']
})

export class ChannelPlayerComponent implements OnInit, OnDestroy {

  public isHideControls: boolean;
  public isDestroyControls: boolean;
  public isShowCheckPin: boolean;
  public currentTvshow: Tvshow;
  public currentTime: number;
  public channelSelectorField = '';

  private channelSelectorTimer: any;
  private channelId: number;
  private autoHideTimer: any;
  private anyKeyEventHandler: any;
  // private keydownEventHandler: any;
  private isPaused: boolean;
  private freezeControlTimer: any;
  private timerSubscription: Subscription;
  private previousTimeStamp: number;
  private readonly freezControlTime = 10000; // milliseconds
  // private pausedTime: number;

  private backServiceSubscriber: Subscription;
  private progressTimerSubscriber: Subscription;
  private playerEventsSubscriber: Subscription;

  private CODE_DIGITS: number[] = [];
  public channels: Observable<Channel[]>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private menuCtrl: MenuControlService,
    private timeService: TimeService,
    private backService: BackService,
    private location: Location,
    private dataService: DataService,
    private channelStore: Store<ChannelsState>,
    private loaderService: LoaderService,
    private playerController: PlayerControllerService,
  ) { }

  ngOnInit() {
    this.menuCtrl.hideMenu();
    this.currentTime = this.timeService.currentTime;
    const id = Number(this.activatedRoute.snapshot.params.id);
    ChannelTapeItemComponent.activeChannelId = id;
    this.channelId = id;
    this.loadTvshowInfo();
    this.playerController.play(id, ContentName.CHANNEL);
    this.autoHideControls();

    this.anyKeyEventHandler = this.showControls.bind(this);
    document.addEventListener('keydown', this.anyKeyEventHandler);
    this.getChannels();

    // this.keydownEventHandler = this.onKeyDown.bind(this);
    // document.addEventListener('keydown', this.keydownEventHandler);

    this.playerEventsSubscribe();

    this.backServiceSubscriber = this.backService.backEvent.subscribe(_ => {
      this.location.back();
    });
    this.CODE_DIGITS = digitsMap;
  }

  private getChannels(): void {
    this.channels = this.channelStore.select('channelsData').pipe(map(data => data.channels));
  }

  private loadTvshowInfo(): void {
    this.dataService.getCurrentTvShows([this.channelId]).then(result => {
      this.currentTvshow = result[0];
    }).finally(() => this.loaderService.loadFinished());
  }

  private playerEventsSubscribe(): void {
    this.playerEventsSubscriber = this.playerController.events.subscribe(res => {
      switch (res) {
        case PlayerEvents.PLAYER_ERROR_LOGIN:
          document.removeEventListener('keydown', this.anyKeyEventHandler);
          this.isDestroyControls = true;
          break;

        case PlayerEvents.PLAYER_ERROR_SUBSCRIPTION:
          document.removeEventListener('keydown', this.anyKeyEventHandler);
          this.isDestroyControls = true;
          break;

        case PlayerEvents.PLAYER_ADULT_CONTENT:
          document.removeEventListener('keydown', this.anyKeyEventHandler);
          this.isDestroyControls = true;
          this.isShowCheckPin = true;
          break;

        case PlayerEvents.PLAYER_VALID_PIN:
          this.anyKeyEventHandler = this.showControls.bind(this);
          document.addEventListener('keydown', this.anyKeyEventHandler);
          this.isDestroyControls = false;
          this.isShowCheckPin = false;
          break;

        case PlayerEvents.PLAYER_PAUSE:
          this.isPaused = true;
          clearTimeout(this.freezeControlTimer);
          break;

        case PlayerEvents.PLAYER_PLAY:
          this.isPaused = false;
          break;

        case PlayerEvents.PLAYER_READY:
          if (this.timerSubscription) {
            this.timerSubscription.unsubscribe();
          }
          clearTimeout(this.freezeControlTimer);
          this.timerSubscription = this.timeService.timeControllerFast.subscribe(currentTime => {
            if (this.previousTimeStamp !== this.playerController.currentTime) {
              this.previousTimeStamp = this.playerController.currentTime;
              this.freezeControl();
              this.currentTime = currentTime;
            }
          });
          break;

        default:
          break;
      }
    });
  }
  /*
    private onKeyDown(event: KeyboardEvent): void {
      this.autoHideControls();
      const code = event.keyCode;
      switch (code) {
        /* case KeyMap.RIGHT:
          this.seekRight();
          break;

        case KeyMap.LEFT:
          this.seekLeft();
          break;
        case KeyMap.ENTER:
          /* if (this.isSeekingEnabled) {
            this.seekActivate();
          } else {
          console.log('Enter');
          if (this.isPaused) {
            // this.playerController.play()
          } else {
            this.playerController.pause();
          }
          // }
          break;

        case KeyMap.PAUSE:
          if (!this.isPaused) {
            this.playerController.pause();
            this.isPaused = true;
            this.pausedTime = this.timeService.currentTime;
          }
          break;

        /* case KeyMap.STOP:
          this.location.back();
          break;

        case KeyMap.PLAY:
          if (this.isPaused) {
            this.playerController.resume();
          }
          break;

        case KeyMap.PLAY_PAUSE:
          /* if (this.isSeekingEnabled) {
            this.seekActivate();
          } else {
          console.log('Or here');
          if (this.isPaused) {
            this.playerController.resume();
          } else {
            this.playerController.pause();
          }
          // }
          break;
        /*
              case KeyMap.RWD:
                this.seekLeft();
                break;

              case KeyMap.FWD:
                this.seekRight();
                break; */

  /* case KeyMap.BACK:
    this.location.back();
    break;

  default:
    break;
}
} */

  private freezeControl(): void {
    clearTimeout(this.freezeControlTimer);
    this.freezeControlTimer = setTimeout(() => {
      this.playerController.stop();
      this.playerController.play(this.channelId, ContentName.CHANNEL);
    }, this.freezControlTime);
  }

  private showControls(event: KeyboardEvent): void {
    const keyCode = event.keyCode;
    if (this.CODE_DIGITS.includes(keyCode)) {
      this.channelDigitSelectorShow(keyCode);
    }
    this.autoHideControls();
  }

  private channelDigitSelectorShow(code: number): void {
    this.channelSelectorField += this.CODE_DIGITS.indexOf(code);
    if (this.channelSelectorField.length < 3) {
      clearTimeout(this.channelSelectorTimer);
      this.channelSelectorTimer = setTimeout(() => {
        this.changeChannelBySelector();
      }, 2000);
    } else {
      clearTimeout(this.channelSelectorTimer);
      setTimeout(() => {
        this.changeChannelBySelector();
      }, 200);
    }
  }

  private changeChannelBySelector(): void {
    const chId = this.getChannelId(this.channelSelectorField);
    if (chId) {
      this.channelChange(chId);
      // ChannelTapeItemComponent.focusChannelIndex = Number(this.channelSelectorField);
    }
    this.channelSelectorField = '';
  }

  private get channelsLength(): number {
    let length = 0;
    this.channelStore.select('channelsData').pipe(take(1)).subscribe(data => length = data.channels.length);
    return length;
  }

  private getChannelId(chNumber: string): number {
    const channelNumber = Number(chNumber);
    if (channelNumber !== 0 && this.channelsLength >= channelNumber) {
      let id = null;
      this.channels.pipe(take(1)).subscribe(channels => id = channels[channelNumber - 1].channel_id);
      return id;
    }
    return null;
  }

  public get isHaveChannelNumber(): boolean {
    return this.channelSelectorField.length > 0;
  }

  public autoHideControls(): void {
    this.isHideControls = false;
    clearTimeout(this.autoHideTimer);
    this.autoHideTimer = setTimeout(() => {
      this.isHideControls = true;
    }, 5000);
  }

  public channelChange(id: number): void {
    if (ChannelTapeItemComponent.activeChannelId !== id) {
      this.loaderService.startLoading(1);
      this.channelId = id;
      this.playerController.updateChannelId(id);
      ChannelTapeItemComponent.activeChannelId = id;
      this.loadTvshowInfo();
    }
  }

  ngOnDestroy() {
    this.playerController.stop();
    this.menuCtrl.showMenu();
    this.isDestroyControls = false;
    this.isShowCheckPin = false;
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
      clearTimeout(this.freezeControlTimer);
    }
    if (this.progressTimerSubscriber) {
      this.progressTimerSubscriber.unsubscribe();
    }
    clearTimeout(this.freezeControlTimer);
    document.removeEventListener('keydown', this.anyKeyEventHandler);
    if (this.backServiceSubscriber) {
      this.backServiceSubscriber.unsubscribe();
    }
    if (this.playerEventsSubscriber) {
      this.playerEventsSubscriber.unsubscribe();
    }
    this.loaderService.loadFinished();
  }
}
