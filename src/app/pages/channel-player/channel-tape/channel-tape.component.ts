import { KeyMap } from '../../../../keymaps/keymap';
import { ChannelTapeItemComponent } from './channel-tape-item/channel-tape-item.component';
import { Channel } from '@models/core';
import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import * as scrollIV from 'scroll-into-view';
import { PositionMemoryService } from '@services/core';

@Component({
  selector: 'app-channel-tape',
  templateUrl: './channel-tape.component.html',
  styleUrls: ['./channel-tape.component.scss']
})

export class ChannelTapeComponent implements OnInit, OnDestroy {

  @Input() channels: Channel[];
  @Output() changeChannel: EventEmitter<number> = new EventEmitter<number>();

  private keydownEventHandler: any;

  constructor(private memoryService: PositionMemoryService) { }

  ngOnInit() {
    const activeChannel = this.channels.find(ch => ch.channel_id === ChannelTapeItemComponent.activeChannelId);
    ChannelTapeItemComponent.focusChannelIndex = this.channels.indexOf(activeChannel);
    setTimeout(() => {
      this.scrollToView();
    }, 1000);
    this.keydownEventHandler = this.onKeyDown.bind(this);
    document.addEventListener('keydown', this.keydownEventHandler);
  }

  private onKeyDown(event: KeyboardEvent): void {
    const code = event.keyCode;
    switch (code) {
      case KeyMap.UP:
        this.navigateUp();
        break;

      case KeyMap.DOWN:
        this.navigateDown();
        break;

      case KeyMap.ENTER:
        this.selectChannel();
        break;

      default:
        break;
    }
  }

  private navigateUp(): void {
    const currentIndex = ChannelTapeItemComponent.focusChannelIndex;
    if (currentIndex > 0) {
      ChannelTapeItemComponent.focusChannelIndex--;
      this.scrollToView();
    }
  }

  private navigateDown(): void {
    const currentIndex = ChannelTapeItemComponent.focusChannelIndex;
    if (currentIndex < this.channels.length - 1) {
      ChannelTapeItemComponent.focusChannelIndex++;
      this.scrollToView();
    }
  }

  private selectChannel(): void {
    const focusChannel = this.channels[ChannelTapeItemComponent.focusChannelIndex];
    if (focusChannel) {
      this.memoryService.currentChannel = focusChannel;
      this.onChannelChange(focusChannel.channel_id);
    }
  }

  private scrollToView(): void {
    const item = document.querySelector('.channel_focus');
    if (item) {
      setTimeout(() => {
        scrollIV(item);
      }, 100);
    }
  }

  public onChannelChange(id: number): void {
    this.changeChannel.next(id);
  }

  ngOnDestroy() {
    document.removeEventListener('keydown', this.keydownEventHandler);
  }

}
