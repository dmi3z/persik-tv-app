import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PlayerControllerService, DataService } from '@services/core';
import { Observable } from 'rxjs';
import { ChannelsState } from '../../../redux/channels/channels.state';
import { Channel, ContentId } from '@models/core';
// import { Book } from '../audiobooks/litres.model';

@Component({
  selector: 'app-home-page',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomePageComponent implements OnInit {

  // public featuredVod: VodSection[] = [];
  // public banners: Observable<Banner[]>;

  // private channelsStoreSubscriber: Subscription;
  // private vodStoreSubscriber: Subscription;
  // private featuredChannelsSubscriber: Subscription;
  // public loadingStep = 0;

  // public mainFeaturedChannels: FeaturedChannelsTape;
  // public featuredChannelsTapes: FeaturedChannelsTape[] = [];
  // public featuredFilmsTapes: FeaturedVideosTape[] = [];
  // public featuredSeriesTapes: FeaturedVideosTape[] = [];
  // public featuredCartoonsTapes: FeaturedVideosTape[] = [];
  // public featuredTvshowsTapes: FeaturedVideosTape[] = [];

  // private channelCategories: Genre[] = [];

  public channels: Observable<Channel[]>;
  // public tvshowIds: Observable<ContentId[]>;
  public filmIds: Observable<ContentId[]>;
  public cartoonIds: Observable<ContentId[]>;
  public serieIds: Observable<ContentId[]>;
  // public courseIds: Observable<ContentId[]>;
  // public books: Observable<Book[]>;
  // public humorIds: Observable<ContentId[]>;
  // public standupIds: Observable<ContentId[]>;

  constructor(
    private channelsStore: Store<ChannelsState>,
    // private vodStore: Store<VodState>,
    private dataService: DataService,
    private playerController: PlayerControllerService,
    // private litresService: LitresService,
    // private booksStore: Store<AudiobooksState>
  ) { }

  ngOnInit() {
    this.playerController.stop();
    this.loadChannels();
    // this.loadTvshows();
    this.loadFilms();
    this.loadCartoons();
    this.loadSeries();
    // this.loadCourses();
    // this.loadAudiobooks();
    // this.loadHumor();
    // this.loadStandup();
    // this.focusOnFirst();
    /* this.channelsStoreSubscriber = this.channelsStore.select('channelsData').subscribe(res => {
      if (res.categories && res.categories.length > 0 && res.channels && res.channels.length > 0 &&
        res.featured_channels && res.featured_channels.length > 0 && !this.mainFeaturedChannels) {
          this.channelCategories = res.categories;
          const featuredChannels = res.channels.filter(channel => {
            return res.featured_channels.filter(featured => featured.channel_id === channel.channel_id).length === 1;
          });
          this.mainFeaturedChannels = {
            title: 'Популярные каналы',
            channels: featuredChannels,
            genre_id: 0
          };
          this.loadChannelsForFeatured();
      }
    });*/
    // this.loadBanners();

    // this.vodStoreSubscriber = this.vodStore.select('vodCategories').subscribe(res => {
    //   if (res.featured && res.featured.sections.length > 0 && this.featuredVod.length === 0) {
    //     this.featuredVod = res.featured.sections;
    //   }
    // });
  }

  private loadChannels(): void {
    this.channels = this.channelsStore.select('channelsData').pipe(map(data => {
      this.focusOnFirst();
      return data.channels.slice(0, 10);
    }));
  }

  /* private loadTvshows(): void {
    this.tvshowIds = this.dataService.loadFeaturedContent(4, 655).pipe(map(data => data.videos));
  } */

  private loadFilms(): void {
    this.filmIds = this.dataService.loadFeaturedContent(1, 881).pipe(map(data => data.videos));
  }

  private loadCartoons(): void {
    this.cartoonIds = this.dataService.loadFeaturedContent(3, 890).pipe(map(data => data.videos));
  }

  private loadSeries(): void {
    this.serieIds = this.dataService.loadFeaturedContent(2, 886).pipe(map(data => data.videos));
  }

  /* private loadCourses(): void {
    this.courseIds = this.dataService.loadFeaturedContent(6, 981).pipe(map(data => data.videos));
  }

  private loadAudiobooks(): void {
    this.litresService.getList(-1, 0, 10);
    this.books = this.booksStore.select('audiobooksData').pipe(map(data => data.books));
  }

  private loadHumor(): void {
    this.humorIds = this.dataService.loadFeaturedContent(4, 96).pipe(map(data => data.videos));
  }

  private loadStandup(): void {
    this.standupIds = this.dataService.loadFeaturedContent(4, 471).pipe(map(data => data.videos));
  } */

  // private loadBanners(): void {
  // this.banners = this.dataService.getBanners();
  // }

  /* public get isHaveFeaturedChannelTapes(): boolean {
    return this.featuredChannelsTapes.length > 0;
  } */

  // public loadNext(): void {
  //   this.loaderService.startLoading();
  //   this.loadingStep += 1;
  //   switch (this.loadingStep) {
  //     case 1:
  //       this.loadFilmsTapes();
  //       break;
  //     case 2:
  //       this.loadSeriesTapes();
  //       break;
  //     case 3:
  //       this.loadCartoonsTapes();
  //       break;
  //     case 4:
  //       this.loadTvshowsTapes();
  //       break;
  //     default:
  //       this.loaderService.stopLoading();
  //       break;
  //   }
  // }

  /* private loadChannelsForFeatured(): void {
    const genreIds = [602, 98, 99, 100, 102, 103, 105];
    const genres: Genre[] = [];
    genreIds.forEach(g => {
      const genre = this.channelCategories.find(cat => cat.id === g);
      if (genre) {
        genres.push(genre);
      }
    });
    this.getChannelsForFeatured(genres).then(res => {
      this.featuredChannelsTapes = res;
      this.loaderService.stopLoading();
      this.focusOnFirst();
    });
  } */

  /* getChannelsForFeatured(genres: Genre[]): Promise<FeaturedChannelsTape[]> {
    return new Promise(resolve => {
      if (genres.length > 0) {
        const fChs: FeaturedChannelsTape[] = [];
        this.featuredChannelsSubscriber = this.channelsStore.select('channelsData').subscribe(chs => {
          if (chs.channels && chs.channels.length > 0) {
            for (const genre of genres) {
              const channels = chs.channels.filter(ch => ch.genres.some(g => +g === +genre.id));
              if (channels && channels.length > 0) {
                fChs.push({
                  genre_id: genre.id,
                  title: genre.name,
                  channels
                });
              }
            }
            resolve(fChs);
          }
        });
      }
    });
  } */

  // private loadFilmsTapes(): void {
  //   this.vodStore.select('vodCategories').subscribe(cats => {
  //     const filmGenreIds: number[] = [
  //       1014, 1023, 1007, 1010, 1009, 1011, 1012, 1008, 1013, 1157, 1158,
  //       1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169
  //     ];
  //     const allGenresArray: Genre[][] = cats.categories.map(cat => cat.genres);
  //     const allgenres: Genre[] = [];
  //     for (const genresArray of allGenresArray) {
  //       allgenres.push(...genresArray);
  //     }
  //     const genres = filmGenreIds.map(gfi => {
  //       const genre = allgenres.find(g => g.id === gfi);
  //       return genre;
  //     });
  //     this.loadFilmsWithCategory(genres);
  //     this.loaderService.stopLoading();
  //   });
  // }

  // private loadFilmsWithCategory(genres: Genre[]): void {
  //   for (let i = 0; i < genres.length - 1; i++) {
  //     this.dataService.getVideoContent(1, genres[i].id, 10).then(v => {
  //       this.featuredFilmsTapes[i] = {
  //         genre_id: genres[i].id,
  //         title: genres[i].name,
  //         videos: v.videos
  //       };
  //     });
  //   }
  // }

  // private loadSeriesTapes(): void {
  //   this.vodStore.select('vodCategories').subscribe(cats => {
  //     const seriesGenreIds: number[] = [1024, 1015, 1170, 1172, 1173, 1174, 1175];
  //     const allGenresArray: Genre[][] = cats.categories.map(cat => cat.genres);
  //     const allgenres: Genre[] = [];
  //     for (const genresArray of allGenresArray) {
  //       allgenres.push(...genresArray);
  //     }
  //     const genres = seriesGenreIds.map(gfi => {
  //       const genre = allgenres.find(g => g.id === gfi);
  //       return genre;
  //     });
  //     this.loadSeriesWithCategory(genres);
  //     this.loaderService.stopLoading();
  //   });
  // }

  // private loadSeriesWithCategory(genres: Genre[]): void {
  //   for (let i = 0; i < genres.length - 1; i++) {
  //     this.dataService.getVideoContent(2, genres[i].id, 10).then(v => {
  //       this.featuredSeriesTapes[i] = {
  //         genre_id: genres[i].id,
  //         title: genres[i].name,
  //         videos: v.videos
  //       };
  //     });
  //   }
  // }

  // private loadCartoonsTapes(): void {
  //   this.vodStore.select('vodCategories').subscribe(cats => {
  //     const cartoonsGnreIds: number[] = [1016, 1155, 1156];
  //     const allGenresArray: Genre[][] = cats.categories.map(cat => cat.genres);
  //     const allgenres: Genre[] = [];
  //     for (const genresArray of allGenresArray) {
  //       allgenres.push(...genresArray);
  //     }
  //     const genres = cartoonsGnreIds.map(gfi => {
  //       const genre = allgenres.find(g => g.id === gfi);
  //       return genre;
  //     });
  //     this.loadCartoonsWithCategory(genres);
  //     this.loaderService.stopLoading();
  //   });
  // }

  // private loadCartoonsWithCategory(genres: Genre[]): void {
  //   for (let i = 0; i < genres.length - 1; i++) {
  //     this.dataService.getVideoContent(3, genres[i].id, 10).then(v => {
  //       this.featuredCartoonsTapes[i] = {
  //         genre_id: genres[i].id,
  //         title: genres[i].name,
  //         videos: v.videos
  //       };
  //     });
  //   }
  // }

  // private loadTvshowsTapes(): void {
  //   this.vodStore.select('vodCategories').subscribe(cats => {
  //     const tvshowGnreIds: number[] = [655, 447, 449, 96, 892];
  //     const allGenresArray: Genre[][] = cats.categories.map(cat => cat.genres);
  //     const allgenres: Genre[] = [];
  //     for (const genresArray of allGenresArray) {
  //       allgenres.push(...genresArray);
  //     }
  //     const genres = tvshowGnreIds.map(gfi => {
  //       const genre = allgenres.find(g => g.id === gfi);
  //       return genre;
  //     });
  //     this.loadTvshowsWithCategory(genres);
  //     this.loaderService.stopLoading();
  //   });
  // }

  // private loadTvshowsWithCategory(genres: Genre[]): void {
  //   for (let i = 0; i < genres.length - 1; i++) {
  //     this.dataService.getVideoContent(4, genres[i].id, 10).then(v => {
  //       this.featuredTvshowsTapes[i] = {
  //         genre_id: genres[i].id,
  //         title: genres[i].name,
  //         videos: v.videos
  //       };
  //     });
  //   }
  // }

  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 200);
  }
}

