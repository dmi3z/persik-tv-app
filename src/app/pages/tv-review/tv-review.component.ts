import { BackService } from './../../services/back.service';
import { Subscription } from 'rxjs';
import { TimeService, LoaderService } from '@services/core';
import { Genre, Channel } from '@models/core';
import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChannelsState } from 'src/redux/channels/channels.state';
import { take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tv-review',
  templateUrl: './tv-review.component.html',
  styleUrls: ['./tv-review.component.scss']
})

export class TvReviewPageComponent implements OnInit, OnDestroy {

  private channelCards: Channel[];
  public genres: Genre[] = [];
  private channelsMatrix: ChannelRow[] = [];

  public channelsForRender: ChannelRow[] = [];
  private offsetIndex = 0;
  public currentTime: number;
  public activeGenre: Genre;

  public isShowPreloadArea = true;

  private activatedRouteSubscriber: Subscription;
  private backServiceSubscriber: Subscription;

  constructor(
    private channelStore: Store<ChannelsState>,
    private loaderService: LoaderService,
    private timeService: TimeService,
    private backService: BackService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.backServiceSubscriber = this.backService.backEvent.subscribe(_ => {
      this.backService.goToMain();
    });
    this.activatedRouteSubscriber = this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.genre) {
        this.activeGenre = JSON.parse(params.genre);
      } else {
        this.activeGenre = {
          id: 0,
          is_main: true,
          name: 'Все жанры',
          name_en: ''
        };
      }
      this.channelCards = this.channels;
      this.onGenreChange();
    });
    this.currentTime = this.timeService.currentTime;
  }

  private get channels(): Channel[] {
    let state: Channel[];
    this.channelStore.select('channelsData').pipe(take(1)).subscribe(s => state = s.channels);
    return state;
  }

  public onGenreChange(): void {
    this.offsetIndex = 0;
    this.loaderService.startLoading(1);
    if (this.activeGenre.id !== 0) {
      const filteredChannels = this.channelCards.filter(ch => ch.genres.some(g => g === this.activeGenre.id));
      this.createChannelMatrix(filteredChannels);
    } else {
      this.createChannelMatrix(this.channelCards);
    }
  }

  /* private checkFinishedTvshows(): void {
    if (this.channelCards && this.channelCards.length > 0) {
      this.channelCards.forEach(card => {
        if (card && card.tvshow && card.tvshow.stop && +card.tvshow.stop < this.currentTime) {
          this.dataService.getCurrentTvShows([card.channel.channel_id]).then(result => {
            card.tvshow = result[0];
          });
        }
      });
    }
  } */

  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 100);
  }

  private reloadPreload(): void {
    this.isShowPreloadArea = false;
    setTimeout(() => {
      this.isShowPreloadArea = true;
    }, 100);
  }

  private createChannelMatrix(channelCards: Channel[]): void {
    let row: Channel[] = [];
    this.channelsMatrix = [];
    this.channelsForRender = [];
    this.reloadPreload();
    for (let i = 0; i < channelCards.length; i++) {
      if (i % 4 === 0 && i !== 0) {
        this.channelsMatrix.push({ row });
        row = [];
      }
      row.push(channelCards[i]);
      if (i === channelCards.length - 1) {
        this.channelsMatrix.push({ row });
      }
    }
    this.channelsForRender = this.channelsMatrix.slice(0, 4);
    this.offsetIndex = 4;
    this.focusOnFirst();
    this.loaderService.loadFinished();
  }


  public preloadChannels(): void {
    this.offsetIndex++;
    if (this.channelsMatrix.length > this.offsetIndex) {
      this.channelsForRender.push(this.channelsMatrix[this.offsetIndex]);
    }
  }

  ngOnDestroy() {
    if (this.activatedRouteSubscriber) {
      this.activatedRouteSubscriber.unsubscribe();
    }
    if (this.backServiceSubscriber) {
      this.backServiceSubscriber.unsubscribe();
    }
  }
}


interface ChannelRow {
  row: Channel[];
}
