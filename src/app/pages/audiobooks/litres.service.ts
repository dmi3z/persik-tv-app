import { PreloadAudiobooks, LoadAudiobooks } from './../../../redux/audiobooks/audiobooks.action';
import { Store } from '@ngrx/store';
import { Genre } from './../../models/category';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BookGenre, Book } from './litres.model';
import { AudiobooksState } from 'src/redux/audiobooks/audiobooks.state';
import { LoaderService } from '@services/core';

@Injectable({ providedIn: 'root' })
export class LitresService {

  private readonly LITRES_BASE_URL = 'https://api.persik.by/v2/litres';
  private readonly TRIAL_MP3_URL = 'https://partnersdnld.litres.ru/get_mp3_trial/';
  private readonly COVER_URL = 'https://partnersdnld.litres.ru/pub/c/cover/';

  constructor(private http: HttpClient, private booksStore: Store<AudiobooksState>, private loaderService: LoaderService) { }

  getGenres(): Observable<Genre[]> {
    return this.http.get<BookGenre[]>(this.LITRES_BASE_URL.concat('/genres'))
      .pipe(map(data => {
        const genres: Genre[] = data.map(res => {
          const genre: Genre = {
            id: res.litres_genre_id,
            is_main: true,
            name: res.name,
            name_en: res.token,
            ch_count: null
          };
          return genre;
        });
        genres.unshift({
          id: -1,
          is_main: true,
          name: 'Бестселлеры',
          name_en: 'bestsellers'
        });
        return genres;
      }));
  }

  getList(genreId: number, offset: number, size: number, isPreload = false): void {
    this.loaderService.startLoading(1);
    const paramsData = {
      size,
      offset
    };
    if (genreId !== -1) {
      Object.assign(paramsData, { genre_id: genreId });
    }
    let params = new HttpParams();
    Object.keys(paramsData).forEach(key => {
      params = params.set(key, paramsData[key]);
    });
    this.http.get<Book[]>(this.LITRES_BASE_URL, { params }).toPromise()
      .then(response => {
        if (isPreload) {
          this.booksStore.dispatch(new PreloadAudiobooks(response));
        } else {
          this.booksStore.dispatch(new LoadAudiobooks(response));
        }
      })
      .catch(() => this.loaderService.loadFinished());
  }

  getListPromise(genreId: number, offset: number, size: number): Promise<Book[]> {
    const paramsData = {
      size,
      offset
    };
    if (genreId !== -1) {
      Object.assign(paramsData, { genre_id: genreId });
    }
    let params = new HttpParams();
    Object.keys(paramsData).forEach(key => {
      params = params.set(key, paramsData[key]);
    });
    return this.http.get<Book[]>(this.LITRES_BASE_URL, { params })
      .toPromise();
  }

  getBooksInfo(ids: number[]): Observable<Book[]> {
    let paramString = '';
    ids.forEach(id => {
      paramString += `id[]=${id}&`;
    });
    paramString.slice(-1);
    return this.http.get<Book[]>(this.LITRES_BASE_URL.concat('/item?', paramString));
  }

  getUserBooks(): Observable<Book[]> {
    const params = new HttpParams()
      .set('bought', '1');
    return this.http.get<Book[]>(this.LITRES_BASE_URL, { params });
  }

  getCover(id: number, cover: string): string {
    return this.COVER_URL.concat(id.toString(), '.', cover);
  }

  getTrial(id: number): string {
    return this.TRIAL_MP3_URL.concat(id.toString(), '.mp3');
  }

}
