import { Book } from './../pages/audiobooks/litres.model';
import { Banner, Country, RatingFilter, SearchStorage, Payment, PaymentInfo } from '@models/core';
import { LoadCategories } from './../../redux/vod/vod.action';
import { LoadChannels, LoadChannelCategories, LoadCurrentTvshows } from './../../redux/channels/channels.action';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ChannelsState } from '../../redux/channels/channels.state';
import { Store } from '@ngrx/store';
import {
  Tvshow,
  Channel,
  Genre,
  ChannelStreamData,
  VideoStreamData,
  Video,
  Person,
  Persons,
  Category,
  VideoInterface,
  SearchData
} from '@models/core';
import { map } from 'rxjs/operators';
import { VodState } from 'src/redux/vod/vod.state';
import { TariffData } from '../models/tariff';
import { Observable } from 'rxjs';
import { LoaderService } from './loader.service';


@Injectable()

export class DataService {

  public activeVideo: Video;
  public activeTvshow: Tvshow;
  public activeAudiobook: Book;
  public currentGenres: string[];
  public activeGenreId: number;

  public searchString = '';
  public searchData: SearchStorage = {
    channels: [],
    litres: [],
    tvshows: {
      title: '',
      genre_id: 0,
      videos: null
    },
    videos: {
      title: '',
      genre_id: 0,
      videos: null
    }
  };

  constructor(
    private http: HttpClient,
    private channelsStore: Store<ChannelsState>,
    private vodStore: Store<VodState>,
    private loaderService: LoaderService
  ) { }

  private BASE_URL = 'https://api.persik.by/';

  loadChannels(): void {
    this.loaderService.startLoading(2);
    this.http.get<ChannelsArray>(this.BASE_URL.concat('v2/content/channels')).toPromise().then(data => {
      data.channels.sort((a, b) => {
        return b.rank - a.rank;
      });
      this.loaderService.loadFinished();
      // const mainChannels = data.channels.filter(channel => channel.available);
      const mainChannels = data.channels;
      this.channelsStore.dispatch(new LoadChannels(mainChannels));
      const ids = mainChannels.map(channel => channel.channel_id);
      this.focusOnFirst();
      this.getCurrentTvShows(ids).then(tvshows => {
        this.loaderService.loadFinished();
        this.channelsStore.dispatch(new LoadCurrentTvshows(tvshows));
      });
    });
  }

  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 200);
  }

  getChannelInfo(id: number): Promise<Channel> {
    if (id) {
      const params: HttpParams = new HttpParams().set('id', id.toString());
      return this.http.get<ChannelsArray>(this.BASE_URL.concat('v2/content/channel'), { params })
        .pipe(map(data => data.channels[0])).toPromise();
    }
  }

  getChannelStream(id: number): Promise<ChannelStreamData> {
    const params: HttpParams = new HttpParams().set('id', id.toString());
    return this.http.get<ChannelStreamData>(this.BASE_URL.concat('v1/stream/channel'), { params }).toPromise();
  }

  loadChannelCategories(): void {
    this.loaderService.startLoading(1);
    this.http.get(this.BASE_URL.concat('v2/categories/channel'))
      .toPromise().then((categories: Genre[]) => {
        categories.unshift({
          id: 0,
          name: 'Все жанры',
          name_en: 'All genres',
          is_main: true
        });
        this.channelsStore.dispatch(new LoadChannelCategories(categories));
        this.loaderService.loadFinished();
      });
  }

  getCurrentTvShows(channelIds: number[]): Promise<Tvshow[]> {
    let queryString = '?';
    channelIds.forEach((item, index) => {
      if (index !== channelIds.length - 1) {
        queryString += `channels[]=${item}&`;
      } else {
        queryString += `channels[]=${item}`;
      }
    });
    return this.http.get<any>(this.BASE_URL.concat('v2/epg/onair', queryString)).pipe(map(response => response.tvshows))
      .toPromise();
  }

  getTvshowStream(id: string): Promise<ChannelStreamData> {
    const params: HttpParams = new HttpParams().set('id', id);
    return this.http.get<ChannelStreamData>(this.BASE_URL.concat('v1/stream/show'), { params }).toPromise();
  }

  getVideoStream(id: number): Promise<VideoStreamData> {
    const params: HttpParams = new HttpParams().set('id', id.toString());
    return this.http.get<VideoStreamData>(this.BASE_URL.concat('v1/stream/video'), { params }).toPromise();
  }

  getTvshows(channelId: number): Promise<Tvshow[]> {
    const params: HttpParams = new HttpParams().set('channels[]', channelId.toString()).set('limit', '1000000');
    return this.http.get<TvshowsInterface>(this.BASE_URL.concat('v2/epg/tvshows'), { params })
      .pipe(map(response => response.tvshows.items))
      .toPromise();
  }

  getVideoInfo(id: any): Promise<Video> {
    const params: HttpParams = new HttpParams()
      .set('id[]', id.toString());
    return this.http.get<VideoInformationBackend>(this.BASE_URL.concat('v2/content/video'), { params }).pipe(map(res => {
      if (res && res.videos) {
        return res.videos[0];
      }
      return null;
    })).toPromise();
  }

  loadVideoContent(categoryId: number, genreId: number, size?: number,
    skip?: number, country?: Country, sort?: RatingFilter): Promise<VideoInterface> {
    let type = 'sort';
    if (sort && sort.value.split(' ').length === 2) {
      type = 'year';
    }
    const params: HttpParams = new HttpParams()
      .set('size', (size && size !== 0) ? size.toString() : '')
      .set('offset', (skip && skip !== 0) ? skip.toString() : '')
      .set('category_id', categoryId.toString())
      .set('genre_id', (genreId !== 0 && genreId) ? genreId.toString() : '')
      .set('country_id', (country && country.id !== 0) ? country.id.toString() : '')
      .set('sort', (type === 'sort' && sort && sort.value) ? sort.value : 'last')
      .set('year_from', (type === 'year') ? sort.value.split(' ')[0] : '')
      .set('year_to', (type === 'year') ? sort.value.split(' ')[1] : '');
    return this.http.get<VideoInterface>(this.BASE_URL.concat('v2/content/videos'), { params }).toPromise();
  }

  loadFeaturedContent(categoryId: number, genreId: number): Observable<VideoInterface> {
    const params: HttpParams = new HttpParams()
      .set('size', '10')
      .set('offset', '0')
      .set('category_id', categoryId.toString())
      .set('genre_id', (genreId !== 0 && genreId) ? genreId.toString() : '')
      .set('sort', 'last');
    return this.http.get<VideoInterface>(this.BASE_URL.concat('v2/content/videos'), { params });
  }


  getVideosInfo(ids: any[]): Promise<Video[]> {
    let queryString = '?';
    ids.forEach((item, index) => {
      if (index !== ids.length - 1) {
        queryString += `id[]=${item}&`;
      } else {
        queryString += `id[]=${item}`;
      }
    });
    return this.http.get<VideoInformationBackend>(this.BASE_URL.concat('v2/content/video', queryString)).pipe(map(res => {
      if (res && res.videos) {
        return res.videos;
      }
      return null;
    })).toPromise();
  }

  getTvshowInfo(id: string): Promise<Tvshow> {
    const params: HttpParams = new HttpParams()
      .set('id[]', id);
    return this.http.get<TvshowInformationBackend>(this.BASE_URL.concat('v2/content/tvshow'), { params })
      .pipe(map(res => res.tvshows[0])).toPromise();
  }

  loadActors(ids: number[]): Promise<Person[]> {
    let paramString = '';
    ids.forEach(id => {
      paramString += `id[]=${id}&`;
    });
    paramString.slice(-1);
    return this.http.get<Persons>(this.BASE_URL.concat('v2/content/person?', paramString)).pipe(map(response => {
      return response.persons;
    })).toPromise();
  }

  loadVodCategories(): void {
    this.loaderService.startLoading(1);
    this.http.get(this.BASE_URL.concat('v2/categories/video'))
      .toPromise().then((categories: Category[]) => {
        this.vodStore.dispatch(new LoadCategories(categories));
        this.loaderService.loadFinished();
      });
  }

  // loadVodFeatured() {
  //   this.http.get<VodSections>(this.BASE_URL.concat('v2/featured')).toPromise().then(featured => {
  //     featured.sections.forEach(section => {
  //       this.loadVodHomeContent(section.source).subscribe(res => {
  //         Object.assign(section, res);
  //       });
  //     });
  //     this.vodStore.dispatch(new LoadHomeVod(featured));
  //   });
  // }

  // private loadVodHomeContent(source: VodSource): Observable<VideoInterface> {
  //   const params: HttpParams = new HttpParams()
  //     .set('size', source.params.size.toString())
  //     .set('category_id', source.params.category_id ? source.params.category_id.toString() : '')
  //     .set('genre_id', source.params.genre_id ? source.params.genre_id.toString() : '');
  //   return this.http.get<VideoInterface>(this.BASE_URL.concat(`v${source.version}/${source.module}/${source.action}`), { params });
  // }

  getVideoContent(categoryId: number, genreId: number, size?: number, skip?: number): Promise<VideoInterface> {
    const params: HttpParams = new HttpParams()
      .set('size', (size && size !== 0) ? size.toString() : '')
      .set('offset', (skip && skip !== 0) ? skip.toString() : '')
      .set('category_id', categoryId.toString())
      .set('genre_id', (genreId !== 0 && genreId) ? genreId.toString() : '')
      .set('sort', 'last');
    return this.http.get<VideoInterface>(this.BASE_URL.concat('v2/content/videos'), { params }).toPromise();
  }

  getBanners(): Observable<Banner[]> {
    return this.http.get<Banner[]>(this.BASE_URL.concat('v2/content/banners2')).pipe(map(res => {
      return res.filter(banner => banner.img_url_tv.length > 0);
    }));
  }

  searchContent(text: string): Promise<SearchData> {
    const params: HttpParams = new HttpParams()
      .set('query', text)
      .set('channels', 'true')
      .set('videos', 'true')
      .set('tvshows', 'true');
    return this.http.get<SearchData>(this.BASE_URL.concat('v2/search'), { params }).toPromise();
  }

  searchByAll(): Promise<SearchData> {
    const params: HttpParams = new HttpParams()
      .set('query', 'все');
    return this.http.get<SearchData>(this.BASE_URL.concat('v1/search'), { params }).toPromise();
  }

  getTariffs(): Promise<TariffData> {
    return this.http.get<TariffData>(this.BASE_URL.concat('v2/billing/products')).toPromise();
  }

  createPayment(payment: Payment): Promise<PaymentInfo> {
    const formData: FormData = new FormData();
    formData.append('pay_sys', payment.pay_sys);
    formData.append('product_option_id[]', `${payment.product_option_id.toString()}`);
    return this.http.post<PaymentInfo>(this.BASE_URL.concat('v2/billing/payment'), formData).toPromise();
  }
}


interface ChannelsArray {
  channels: Channel[];
}

interface TvshowsInterface {
  tvshows: {
    items: Tvshow[]
  };
}

interface VideoInformationBackend {
  videos: Video[];
}

interface TvshowInformationBackend {
  tvshows: Tvshow[];
}
