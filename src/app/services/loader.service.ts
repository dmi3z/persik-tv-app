import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()

export class LoaderService {

  public loaderState = new Subject<boolean>();
  private loadersInQueue = 0;

  constructor() {}

  public startLoading(count: number): void {
    this.loadersInQueue += count;
    this.loaderState.next(true);
  }

  public loadFinished(): void {
    this.loadersInQueue--;
    console.log(this.loadersInQueue);
    if (this.loadersInQueue <= 0) {
      this.stopLoading();
    }
  }

  private stopLoading(): void {
    this.loadersInQueue = 0;
    this.loaderState.next(false);
  }

}
