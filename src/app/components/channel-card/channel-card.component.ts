import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Tvshow, Channel } from '@models/core';
import { ImageService } from '@services/core';
import { Router } from '@angular/router';
import { ChannelsState } from 'src/redux/channels/channels.state';

@Component({
  selector: 'app-channel-card',
  templateUrl: './channel-card.component.html',
  styleUrls: ['./channel-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChannelCardComponent implements OnInit {
  @Input() currentTime: number;
  @Input() last: boolean;
  @Input() stub: boolean;
  @Input() channel: Channel;
  @Output() showAllEvent = new EventEmitter<any>();
  public cover: string;
  public isShow: boolean;
  public tvshow: Observable<Tvshow>;

  constructor(
    private imageService: ImageService,
    private router: Router,
    private channelStore: Store<ChannelsState>
  ) { }

  ngOnInit(): void {
    if (!this.isLast && !this.isStub) {
      this.cover = this.imageService.getChannelFrame(this.channel.channel_id, this.currentTime);
      this.loadTvshow();
    }
  }

  private loadTvshow(): void {
    this.tvshow = this.channelStore.select('channelsData').pipe(map(data => {
      const tv = data.currentTvshows.find(tvshow => +tvshow.channel_id === this.channel.channel_id);
      if (tv) {
        return tv;
      }
      const stubTvshow: Tvshow = {
        channel_id: 10,
        start: 0,
        stop: 0,
        title: 'Нет данных',
        date: null,
        deleted: 0,
        ts: 0,
        tvshow_id: null,
        video_id: 0
      };
      return stubTvshow;
    }));
  }

  public playChannel(): void {
    this.router.navigate(['channel-player', this.channel.channel_id]);
  }

  public get isLast(): boolean {
    return this.last;
  }

  public get isStub(): boolean {
    return this.stub;
  }

  public showAll(): void {
    this.showAllEvent.emit();
  }
}
