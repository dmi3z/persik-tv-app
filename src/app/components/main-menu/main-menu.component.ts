// import { MenuControlService } from './../../services/menu-control.service';
import { AuthService } from '@services/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { menus } from './menus';
import { Genre } from '@models/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { VodState } from 'src/redux/vod/vod.state';
import { map } from 'rxjs/operators';
import { ChannelsState } from 'src/redux/channels/channels.state';
import { environment } from 'src/environments/environment.prod';
import { LitresService } from 'src/app/pages/audiobooks/litres.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})

export class MainMenuComponent implements OnInit {

  public favoriteItem: MenuItem = {
    id: 16,
    name: 'Избранное',
    redirect: 'favorite',
    icon: './assets/menu-icons/star-icon.svg'
  };

  public authItem: MenuItem = {
    id: 17,
    name: 'Авторизация',
    redirect: 'auth',
    icon: './assets/menu-icons/sign-in.png'
  };

  public accountItem: MenuItem = {
    id: 18,
    name: 'Личный кабинет',
    redirect: 'account',
    icon: './assets/menu-icons/user-solid.svg'
  };

  public isCertificateMode: boolean;
  public isShowGenreMenu: boolean;

  private idsVodMenuItems: number[] = [1, 2, 3, 4, 6];
  private idsChannelMenuItems: number[] = [7, 8];
  private idsLitresMenuItems: number[] = [9];
  public genres: Observable<Genre[]>;
  // public activeGenre: Genre;
  public activeRoute: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private vodStore: Store<VodState>,
    private channelStore: Store<ChannelsState>,
    private litresService: LitresService
  ) { } // private menuController: MenuControlService

  ngOnInit() {
    /* this.menuController.isCertificateMode()
      .then(res => this.isCertificateMode = res)
      .catch(_ => this.isCertificateMode = false); */
  }

  public onMenuItemFocus(item: MenuItem): void {
    this.isShowGenreMenu = (this.idsVodMenuItems.includes(item.id) || this.idsChannelMenuItems.includes(item.id) || this.idsLitresMenuItems.includes(item.id));
    if (this.isShowGenreMenu) {
      if (this.idsVodMenuItems.includes(item.id)) {
        this.genres = this.vodStore.select('vodCategories').pipe(map(res => {
          let genres = res.categories.find(cat => Number(cat.id) === item.id).genres;
          genres = genres.filter(genre => (genre.is_main && genre.name_en !== 'pladform'));
          genres.unshift({
            id: 0,
            name: 'Все жанры',
            is_main: true,
            name_en: 'All genres'
          });
          return genres;
        }));
      }
      if (this.idsChannelMenuItems.includes(item.id)) {
        this.genres = this.channelStore.select('channelsData').pipe(map(res => {
          let genres = res.categories;
          // ---- for pansonic -----
          if (environment.platform === 'panasonic') {
            const isFsr400 = navigator.userAgent.includes('SW-Version/V8-T56FSPS-LF1V');
            if (isFsr400) {
              genres = genres.filter(genre => genre.id !== 679);
            }
          }
          // -----------------------
          return genres;
        }));
      }
      if (this.idsLitresMenuItems.includes(item.id)) {
        this.genres = this.litresService.getGenres();
      }
      this.activeRoute = item.redirect;
    }
  }

  public onGenreChange(genre: Genre): void {
    this.router.navigate([this.activeRoute], { queryParams: { genre: JSON.stringify(genre) } });
  }

  public onEnterKey(item: MenuItem): void {
    this.router.navigate([item.redirect]);
    this.isShowGenreMenu = false;
    this.focusOnFirst();
  }

  public onMenuItemsBlur(): void {
    setTimeout(() => {
      const item = document.activeElement;
      if (!item.classList.contains('main-menu__item') && !item.classList.contains('genre-filter__item')) {
        this.isShowGenreMenu = false;
      }
    }, 0);
  }

  public get isLogin(): boolean {
    return this.authService.isLogin;
  }

  public get menuItems(): MenuItem[] {
    return this.isCertificateMode ? menus.short : menus.full;
  }

  private focusOnFirst(): void {
    setTimeout(() => {
      const elem: HTMLElement = document.querySelector('.page [nav-group]');
      if (elem) {
        elem.focus();
      }
    }, 200);
  }

}

interface MenuItem {
  id: number;
  name: string;
  redirect: string;
  icon: string;
}
