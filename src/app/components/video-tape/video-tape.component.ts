import { ContentId } from './../../models/vod';
import { ContentType, Genre, Category } from '@models/core';
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { VodState } from 'src/redux/vod/vod.state';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-video-tape',
  templateUrl: 'video-tape.component.html',
  styleUrls: ['video-tape.component.scss']
})

export class VideoTapeComponent {

  @Input() tape: ContentId[];
  @Input() type: ContentType;
  @Input() redirectPath: string;
  @Input() isLast: boolean;
  @Input() title: string;
  @Input() countInRow = 5;

  constructor(private router: Router, private vodStore: Store<VodState>) { }

  public onShowAll(): void {
    if (this.redirectPath) {
      // this.dataService.activeGenreId = this.tape.genre_id;
      this.router.navigate([this.redirectPath], {queryParams: { genre: this.getRedirectGenre() }});
    }
  }

  private getRedirectGenre(): string {
    switch (this.redirectPath) {
      case 'films':
        return JSON.stringify(this.categories.find(cat => Number(cat.id) === 1).genres.find(g => Number(g.id) === 881));

      case 'cartoons':
        return JSON.stringify(this.categories.find(cat => Number(cat.id) === 3).genres.find(g => Number(g.id) === 890));

      case 'series':
        return JSON.stringify(this.categories.find(cat => Number(cat.id) === 2).genres.find(g => Number(g.id) === 886));
      default:
        return null;
    }
  }

  private get categories(): Category[] {
    let categories: Category[];
    this.vodStore.select('vodCategories').pipe(take(1)).subscribe(res => categories = res.categories);
    return categories;
  }
}
