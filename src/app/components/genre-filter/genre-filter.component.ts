import { Genre } from '@models/core';
import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-genre-filter',
  templateUrl: './genre-filter.component.html',
  styleUrls: ['./genre-filter.component.scss']
})

export class GenreFilterComponent {

  @Input() genres: Genre[];
  @Input() activeGenre: Genre;
  @Output() genreChange: EventEmitter<Genre> = new EventEmitter<Genre>();
  @Output() blurEvent = new EventEmitter<any>();

  constructor() {}

  public selectGenre(genre: Genre): void {
    if (this.activeGenre !== genre) {
      this.activeGenre = genre;
      this.genreChange.emit(this.activeGenre);
    }
  }

  public isGenreActive(genre: Genre): boolean {
    if (this.activeGenre && genre) {
      return this.activeGenre.id === genre.id;
    }
    return false;
  }

  public onBlur(): void {
    this.blurEvent.next(new Date().getTime);
  }
}
